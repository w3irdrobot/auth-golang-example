package db

import (
	"context"
	"io"

	authgolangexample "gitlab.com/searsaw/auth-golang-example"
)

type DB interface {
	io.Closer
	CreateUser(ctx context.Context, user *authgolangexample.User) (*authgolangexample.User, error)
	GetUserByUsername(ctx context.Context, username string) (*authgolangexample.User, error)
}
