package sqlite

import (
	"context"
	"fmt"
	"os"
	"testing"

	authgolangexample "gitlab.com/searsaw/auth-golang-example"
)

const (
	dbFilename = "test.db"
)

func TestMain(m *testing.M) {
	code := m.Run()

	if err := os.Remove(dbFilename); err != nil && !os.IsNotExist(err) {
		fmt.Printf("error removing the database file: %s\n", err)
		os.Exit(1)
	}

	os.Exit(code)
}

func TestNewSqlite(t *testing.T) {
	if _, err := NewSqlite(dbFilename); err != nil {
		t.Fatalf("error creating the SQLite connection: %s", err)
	}
}

func TestSqlite_CreateUser(t *testing.T) {
	db, err := NewSqlite(dbFilename)
	if err != nil {
		t.Fatalf("error creating the SQLite connection: %s", err)
	}

	user := &authgolangexample.User{
		FirstName: "Alex",
		LastName:  "Sears",
		Username:  "alex.sears",
		Password:  "somethinghashed",
	}
	user, err = db.CreateUser(context.Background(), user)
	if err != nil {
		t.Fatalf("error creating the user: %s", err)
	}

	if user.ID == 0 {
		t.Error("the ID is not set")
	}

	if user.Password == "somethinghashed" {
		t.Error("the password after creating should not be equal to the original password due to hashing")
	}
}

func TestSqlite_GetUserByUsername(t *testing.T) {
	db, err := NewSqlite(dbFilename)
	if err != nil {
		t.Fatalf("error creating the SQLite connection: %s", err)
	}

	user := &authgolangexample.User{
		FirstName: "Alex",
		LastName:  "Sears",
		Username:  "alex.sears",
		Password:  "somethinghashed",
	}
	user, err = db.CreateUser(context.Background(), user)
	if err != nil {
		t.Fatalf("error creating the user: %s", err)
	}

	user2, err := db.GetUserByUsername(context.Background(), user.Username)
	if err != nil {
		t.Fatalf("error getting the user: %s", err)
	}

	if user2.FirstName != user.FirstName || user2.LastName != user.LastName {
		t.Errorf("the users are not equal. original: %v, queried: %v", user, user2)
	}
}
