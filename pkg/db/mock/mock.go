package mock

import (
	"context"
	"errors"
	"fmt"

	authgolangexample "gitlab.com/searsaw/auth-golang-example"
)

type MockDb struct {
	users []*authgolangexample.User
}

func NewMockDb() *MockDb {
	return &MockDb{}
}

func (m *MockDb) Close() error {
	return nil
}

func (m *MockDb) CreateUser(ctx context.Context, user *authgolangexample.User) (*authgolangexample.User, error) {
	hashed, err := user.HashedPassword()
	if err != nil {
		return nil, fmt.Errorf("error hashing the password: %w", err)
	}
	user.Password = hashed
	user.ID = int64(len(m.users) + 1)

	m.users = append(m.users, user)

	return user, nil
}

func (m *MockDb) GetUserByUsername(ctx context.Context, username string) (*authgolangexample.User, error) {
	var user *authgolangexample.User
	for _, u := range m.users {
		if u.Username == username {
			user = u
			break
		}
	}

	if user == nil {
		return nil, errors.New("user not found")
	}
	return user, nil
}
