package controller

import (
	"context"
	"testing"

	authgolangexample "gitlab.com/searsaw/auth-golang-example"
	"gitlab.com/searsaw/auth-golang-example/pkg/db/mock"
)

func TestAuthController_HandleRegister(t *testing.T) {
	dal := mock.NewMockDb()
	controller := NewAuthController(dal)
	user := &authgolangexample.User{
		FirstName: "Alex",
		LastName:  "Sears",
		Username:  "alex.sears",
		Password:  "qwertyuiop",
	}
	u, err := controller.HandleRegister(context.Background(), user)
	if err != nil {
		t.Fatalf("error registering the user: %s", err)
	}

	if u.ID == 0 {
		t.Fatal("the user's ID was not set")
	}
}

func TestAuthController_HandleLogin(t *testing.T) {
	dal := mock.NewMockDb()
	controller := NewAuthController(dal)
	user := &authgolangexample.User{
		FirstName: "Alex",
		LastName:  "Sears",
		Username:  "alex.sears",
		Password:  "qwertyuiop",
	}
	_, err := dal.CreateUser(context.Background(), user)
	if err != nil {
		t.Fatalf("error creating the user: %s", err)
	}

	user2, err := controller.HandleLogin(context.Background(), "alex.sears", "qwertyuiop")
	if err != nil {
		t.Fatalf("error logging the user in: %s", err)
	}

	if user2.FirstName != user.FirstName || user2.LastName != user.LastName {
		t.Errorf("the users are not equal. original: %v, queried: %v", user, user2)
	}
}
