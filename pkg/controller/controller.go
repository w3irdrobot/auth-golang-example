package controller

import (
	"context"
	"errors"
	"fmt"

	authgolangexample "gitlab.com/searsaw/auth-golang-example"
	"gitlab.com/searsaw/auth-golang-example/pkg/db"
)

// AuthController represents the logic for authorization in the program
type AuthController struct {
	dal db.DB
}

var (
	// ErrIncorrectPassword represents when a password given
	// user's password is incorrect
	ErrIncorrectPassword = errors.New("the given password is incorrect")
)

// NewAuthController creates a new AuthController using the given DB
func NewAuthController(dal db.DB) *AuthController {
	return &AuthController{dal: dal}
}

// HandleLogin queries the database and checks the password for the given username
// and returns the user if the correct password is given and returns an error if
// the passwords don't match or the user doesn't exist
func (lc *AuthController) HandleLogin(ctx context.Context, username, password string) (*authgolangexample.User, error) {
	user, err := lc.dal.GetUserByUsername(ctx, username)
	if err != nil {
		return nil, fmt.Errorf("error getting user by username: %w", err)
	}

	err = user.CheckPassword(password)
	if err == authgolangexample.ErrIncorrectPassword {
		return nil, ErrIncorrectPassword
	}
	if err != nil {
		return nil, fmt.Errorf("error checking the user's password: %w", err)
	}

	return user, nil
}

// HandleRegister saves the given user into the database and returns the
// user with the ID set inside it
func (lc *AuthController) HandleRegister(ctx context.Context, user *authgolangexample.User) (*authgolangexample.User, error) {
	user, err := lc.dal.CreateUser(ctx, user)
	if err != nil {
		return nil, fmt.Errorf("error creating the user: %w", err)
	}
	return user, nil
}
