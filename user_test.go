package authgolangexample

import (
	"testing"
)

func TestUser_HashedPassword(t *testing.T) {
	user := User{Password: "helloworld"}

	hashed, err := user.HashedPassword()
	if err != nil {
		t.Fatalf("error hashing the password: %s", err)
	}

	if hashed == user.Password {
		t.Errorf("the hashed password should not be equal to the password. password: %s, hashed: %s", user.Password, hashed)
	}
}

func TestUser_CheckPassword(t *testing.T) {
	password := "helloworld"
	user := User{Password: password}

	hashed, err := user.HashedPassword()
	if err != nil {
		t.Fatalf("error hashing the password: %s", err)
	}
	user.Password = hashed

	if err := user.CheckPassword(password); err != nil {
		t.Errorf("error checking the password: %s", err)
	}
}
