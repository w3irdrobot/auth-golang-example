package authgolangexample

import (
	"errors"
	"fmt"

	"golang.org/x/crypto/bcrypt"
)

var (
	// ErrIncorrectPassword represents when a password given
	// user's password is incorrect
	ErrIncorrectPassword = errors.New("the given password is incorrect")
)

// User represents a user in the system
type User struct {
	ID        int64
	FirstName string
	LastName  string
	Username  string
	Password  string
}

// HashedPassword returns the hashed version of the password in the user struct.
// This should only be called once and only when a new User is created.
func (u *User) HashedPassword() (string, error) {
	hashed, err := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
	if err != nil {
		return "", fmt.Errorf("error generating the password: %s", err)
	}
	return string(hashed), nil
}

// CheckPassword checks the given password against the hashed password in the
// user struct. nil is returned if the passwords match
func (u *User) CheckPassword(password string) error {
	err := bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(password))
	if err == bcrypt.ErrMismatchedHashAndPassword {
		return ErrIncorrectPassword
	}
	if err != nil {
		return fmt.Errorf("error comparing the hash and password: %w", err)
	}
	return nil
}
